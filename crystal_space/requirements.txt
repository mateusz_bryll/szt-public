defusedxml==0.4.1
Django==1.9
django-allauth==0.29.0
django-render-partial==0.1
django-suit==0.2.23
oauthlib==2.0.1
pip-autoremove==0.9.0
python3-openid==3.0.10
requests==2.12.4
requests-oauthlib==0.7.0
