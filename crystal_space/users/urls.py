from django.conf.urls import url, include
from .views import *

app_name = 'users'

urlpatterns = [
    url(r'^login/', UserLoginView.as_view(), name='login'),
    url(r'^logout/', logout_user, name='logout'),
    url(r'^register/', UserRegisterView.as_view(), name='register'),
]