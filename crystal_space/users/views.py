from django.shortcuts import render, redirect
from django.views.generic import View, ListView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from .forms import UserLoginForm


def logout_user(request):
        logout(request);
        return redirect('users:login')


class UserLoginView(View):
    form_class = UserLoginForm
    template_name = 'users/login_form.html'

    def get(self, request):
        if request.user.is_authenticated and not request.user.is_anonymous():
            return redirect('home:index')

        return render(request, self.template_name, {'form': self.form_class(None)})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('home:index')
            
            return render(request, self.template_name, {'form': form, 'error_message': 'Invalid username or password.'})

        return render(request, self.template_name, {'form': form})


class UserRegisterView(View):
    form_class = UserCreationForm
    template_name = 'users/register_form.html'

    def get(self, request):
        if request.user.is_authenticated and not request.user.is_anonymous():
            return redirect('home:index')

        return render(request, self.template_name, {'form': self.form_class(None)})
    
    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return redirect('users:login')
        return render(request, self.template_name, {'form': form})
