from django import forms


class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True, label='Your name')
    contact_email = forms.EmailField(required=True, label='Your email')
    content = forms.CharField(required=True, widget=forms.Textarea, label='Message')