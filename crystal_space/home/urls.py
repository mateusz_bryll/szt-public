from django.conf.urls import url
from .views import *


app_name = 'home'

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^contact/$', ContactView.as_view(), name='contact'),
    url(r'^privacy-policy/$', PrivacyPolicyView.as_view(), name='privacy_policy'),
    url(r'^terms-and-conditions/$', TermsAndConditionsView.as_view(), name='terms_conditions'),
    url(r'^home-slider/$', HomeSliderView.as_view(), name='home_slider'),
    url(r'^newsletter_signin/$', NewsletterSigninView.as_view(), name='newsletter_signin'),
]