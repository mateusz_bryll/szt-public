from django.shortcuts import render, redirect
from django.views.generic import View
from django.views.generic.edit import CreateView
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context
from django.contrib.auth.models import User
from .forms import *
from .models import *


def ParseList(content):
    text = content.replace('\r\n', '')
    result = []
    for row in text[1:-1].split('),('):
        result_item = row.split('|')
        result.append(ListItemModel(result_item[0], result_item[1], result_item[2]))
    return result


class IndexView(View):
    template_name = 'home/index.html'

    def get(self, request):
        article_list = HomePageArticle.objects.filter(display=True).order_by('index', '-creation_date')
        return render(request, self.template_name, {'article_list': article_list})


class ContactView(View):
    form_class = ContactForm
    template_name = 'home/contact.html'

    def get(self, request):
        return render(request, self.template_name, {'form': self.form_class(None)})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            contact_name = form.cleaned_data['contact_name']
            contact_email = form.cleaned_data['contact_email']
            content = form.cleaned_data['content']

            template = get_template('home/_contact_template.txt')
            context = Context({
                'contact_name': contact_name,
                'contact_email': contact_email,
                'content': content
            })
            email_body = template.render(context)

            email = EmailMessage(
                'New message from CristalSpace.cloud',
                email_body,
                'CrystalSpace.cloud',
                [User.objects.get(username='Administrator').email],
                headers = {'Replay-To': contact_email}
            )
            email.send()

            log_entry = ContactFormMessageLog(sender_name=contact_name, sender_email=contact_email, message=email_body)
            log_entry.save()

            return redirect('home:index')
        return render(request, self.template_name, {'form': form})


class PrivacyPolicyView(View):
    template_name = 'home/privacy_policy.html'

    def get(self, request):
        policy = Regulations.objects.filter(name="Privacy policy")[:1].get().content
        privacy_policy = ParseList(policy)
        return render(request, self.template_name, {'privacy_policy': privacy_policy})


class TermsAndConditionsView(View):
    template_name = 'home/terms_conditions.html'

    def get(self, request):
        terms = Regulations.objects.filter(name="Terms and conditions")[:1].get().content
        terms_and_conditions = ParseList(terms)
        return render(request, self.template_name, {'terms_and_conditions': terms_and_conditions})


class HomeSliderView(View):
    template_name = 'home/_home_page_slider.html'

    def get(self, request):
        carousel_items = CarouselItem.objects.filter(display=True).order_by('index', '-creation_date')
        return render(request, self.template_name, {'carousel_items': carousel_items})


class NewsletterSigninView(CreateView):
    model = MailingList
    fields = ['email']
    success_url = '/'