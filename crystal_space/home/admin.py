from django.contrib import admin
from .models import *


admin.site.register(CarouselItem)
admin.site.register(HomePageArticle)
admin.site.register(ContactFormMessageLog)
admin.site.register(Regulations)
admin.site.register(MailingList)
