from django.db import models

class CarouselItem(models.Model):
    name = models.CharField(max_length=255)
    picture = models.FileField(upload_to='./home/slider/')
    index = models.PositiveSmallIntegerField(default=0)
    display = models.BooleanField(default=True)
    creation_date = models.DateField(auto_now_add=True)

    readonly_fields = ('creation_date')

    def __str__(self):
        return self.name


class HomePageArticle(models.Model):
    title = models.CharField(max_length=48)
    article_text = models.TextField()
    picture = models.FileField(upload_to='./home/articles/')
    index = models.PositiveSmallIntegerField(default=0)
    display = models.BooleanField(default=True)
    creation_date = models.DateField(auto_now_add=True)

    readonly_fields = ('creation_date')

    def __str__(self):
        return self.title


class ContactFormMessageLog(models.Model):
    sender_name = models.CharField(max_length=200)
    sender_email = models.EmailField()
    message = models.TextField()

    def __str__(self):
        return self.message[0:150]


class Regulations(models.Model):
    name = models.CharField(max_length=50)
    content = models.TextField()

    def __str__(self):
        return self.name;


class MailingList(models.Model):
    email = models.EmailField()
    signin_date = models.DateField(auto_now_add=True)

    readonly_fields = ('sigin_date')

    def __str__(self):
        return self.email + ' (' + self.signin_date.strftime("%d.%m.%Y") + ')'


class ListItemModel():
    def __init__(self, number, header, content):
        self.number = number
        self.header = header
        self.content = content
