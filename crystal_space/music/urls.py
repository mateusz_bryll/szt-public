from django.conf.urls import url
from .views import *

app_name = 'music'

urlpatterns = [
    url(r'^albums/$', AlbumListView.as_view(), name='albums'),
    url(r'^albums/play/(?P<pk>[0-9]+)/$', AlbumDetailView.as_view(), name='album_detail'),
    url(r'^albums/add/$', AlbumCreateView.as_view(), name='album_create'),
    url(r'^albums/(?P<pk>[0-9]+)/$', AlbumUpdateView.as_view(), name='album_update'),
    url(r'^albums/(?P<pk>[0-9]+)/delete/$', AlbumDeleteView.as_view(), name='album_delete'),
    url(r'^songs/add/(?P<album_pk>[0-9]+)/$', SongCreateView.as_view(), name='song_create'),
    url(r'^songs/(?P<pk>[0-9]+)/$', SongUpdateView.as_view(), name='song_update'),
    url(r'^songs/(?P<pk>[0-9]+)/delete/$', SongDeleteView.as_view(), name='song_delete'),
]