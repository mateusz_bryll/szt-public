(function(player, $, undefinied) {
    var current = 0;
    var audio = $('#audio')[0];
    var playlist = $('#playlist');
    var tracks = playlist.find('tr');
    var len = tracks.length;
    var titleTag = $('#song-title');
    var songInfoTag = $('#song-info');

    function run(link, player) {
        player.src = link.attr('data-player-track');
        link.addClass('active').siblings().removeClass('active');
        titleTag.text(link.attr('data-player-track-title'));
        songInfoTag.text(link.attr('data-player-track-album') + ' by ' + link.attr('data-player-track-artist'));
        player.load();
        player.play();
    };

    player.playAlbum = function() {
        //audio.play();

        playlist.find('tr').click(function (e) {
            link = $(this);
            current = link.index();
            run(link, audio);
        });

        audio.addEventListener('ended', function(e) {
            current++;
            if (current >= len) {
                current = 0;
                link = playlist.find('tr')[0];
            }
            else {
                link = playlist.find('tr')[current];
            }

            run($(link), audio);
        });
    };

}(window.player = window.player || {}, jQuery));

$(document).ready(function() {
    player.playAlbum();
})