from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db import models


def content_file_name(instance, filename):
    return '/'.join(['user_files', instance.user.username + '_' + str(instance.user.pk), filename])


class Album(models.Model):
    user = models.ForeignKey(User, default=1)
    artist = models.CharField(max_length=25)
    album_title = models.CharField(max_length=25)
    album_logo = models.FileField(upload_to=content_file_name)

    def get_absolute_url(self):
        return reverse('music:album_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.album_title


class Song(models.Model):
    user = models.ForeignKey(User, default=1)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    song_title = models.CharField(max_length=255)
    audio_file = models.FileField(upload_to=content_file_name)

    def get_absolute_url(self):
        return reverse('music:album_detail', kwargs={'pk': str(self.album.pk)})

    def __str__(self):
        return self.song_title
