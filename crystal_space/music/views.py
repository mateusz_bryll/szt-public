from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic import View, DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import *


class AlbumListView(ListView):
    model = Album
    template_name = 'music/albums.html'
    context_object_name = 'album_list'

    def get_queryset(self):
        return Album.objects.filter(user=self.request.user).order_by('album_title')


class AlbumDetailView(DetailView):
    model = Album
    template_name = 'music/songs.html'
    context_object_name = 'album'


class AlbumCreateView(CreateView):
    model = Album
    fields = ['artist', 'album_title', 'album_logo']

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(AlbumCreateView, self).form_valid(form)


class AlbumUpdateView(UpdateView):
    model = Album
    fields = ['artist', 'album_title', 'album_logo']


class AlbumDeleteView(DeleteView):
    model = Album
    success_url = reverse_lazy('music:albums')


class SongCreateView(CreateView):
    model = Song
    fields = ['song_title', 'audio_file']

    #def get_initial(self):
    #    return { 'album': Album.objects.get(pk=self.kwargs['album_pk']) }

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.album = Album.objects.get(pk=self.kwargs['album_pk'])
        return super(SongCreateView, self).form_valid(form)


class SongUpdateView(UpdateView):
    model = Song
    fields = ['song_title', 'audio_file']


class SongDeleteView(DeleteView):
    model = Song

    def get_success_url(self):
        song = Song.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('music:album_detail', kwargs={'pk': str(song.album.pk)})
